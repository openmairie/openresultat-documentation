.. _utilisation:

###########
Utilisation
###########

Une fois le paramètrage métier terminé, il faut préparer l'élection. Les étapes de préparation sont :

#. Configurer l'élection.
#. Ajouter les candidats à l'élection.
#. Créer les centaines.
#. Créer les animations.
#. Ajouter les plans à l'élection.
#. Vérifier et tester le paramétrage.
#. Importer/Saisir les inscrits.
#. Saisir les centaines.
#. Saisir la participation.
#. Saisir les résultats.
#. Publier les résultats et la participation.
#. Calculer le nombre de sièges (élection municipale et métropolitaine).
#. Afficher les animations
#. Afficher la page web
#. Réaliser les éditions et les exports


Pour faciliter, la réalisation de ces étapes un workflow de l'élection a été mis en place.
Ainsi l'élection est découpée en cinq étapes :

1. paramètrage
2. simulation
3. saisie
4. finalisation
5. archivage

Dans ce chapitre, les fonctions et l'utilisation de l'application sont décrites au fur et à mesure de ces étapes.

.. toctree::

    election.rst
    parametrage_utilisation.rst
    simulation.rst
    saisie.rst
    finalisation.rst
    archivage.rst
