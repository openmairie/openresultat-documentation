.. _finalisation:

Étape de finalisation
=====================

L'étape de finalisation interviens une fois que les résultats ont été saisis et validés.
En effet, durant cette étape, les résultats ne peuvent plus être modifiés. Il est donc important de s'assurer que les résultats sont corrects.
Pour s'en assurer l'utilisateur peut consulter le tableau de bord de l'élection.

.. figure:: tb_finalisation.png

   Tableau de bord de l'étape de finalisation

Au cours de cette étape l'utilisateur pourra :

   - Accéder au portail web.
   - Accéder aux animations (et les modifier si besoin).
   - Calculer le nombre de sièges obtenus aux conseils municipal, communautaire et métropolitain.
   - Éditer les résultats et les extraires.
   - Récupérer le fichier csv à envoyer à la préfecture.

.. figure:: portlet_finalisation.png

   Actions disponibles

Portail Web
-----------

Tout au long du processus de saisie de l’élection des fichiers sont transférés au site web de la ville,
pour que les citoyens puissent consulter les résultats en direct de chez eux.

.. figure:: web.png

   Portail web

Cette rubrique nous permet de visualiser en local les fichiers qui sont transférés sur le site Web de la ville.

L'accès au portail web se fait depuis le tableau de bord de l'application.


Affichage des animations
------------------------

Tout au long du processus de saisie et de finalisation de l’élection des fichiers sont transférés à l'animation.
Ces animations sont accessible depuis l'onglet *animation(s)*, en sélectionnant l'animation à visualiser et en cliquant sur *animation* dans le portlet.

.. figure:: portlet_animation.png

   Portlet d'action des animations

Calcul du nombre de sièges
--------------------------

Le calcul des sièges s'effectue en cliquant sur l'action *calcul siège élu* dans le portlet de l'élection.

Cette action calcule le nombre de siéges obtenus pour le conseil municipal, le conseil communautaire et le conseil métropolitain.
Le calcul est effectué avec uniquement les résultats des unités de type bureaux de vote (case à cocher dans le formulaire du type).
Les autres unités et les périmètres ne sont pas pris en compte pour le calcul.

.. note::

   Pour que le calcul s'effectue correctement, il faut remplir dans le formulaire candidat de l'élection l'age moyen de la liste municipale et l'age moyen de la liste communautaire.
   
   Il faut également paramétrer le nombre de sièges à répartir dans la partie *répartition* du formulaire de l'élection.

La repartition se fait au 1er tour si une liste obtient plus de 50% de voix et un quart au moins des electeurs inscrits
Il y a un message si la 1ère liste n'a pas la majorite absolue

Les listes < 5% des exprimés sont éliminées.

Le bonus est attribué à la 1ère liste et il est égal à la moitié arrondi à l'entier superieur si il y a plus de 4 sièges à repartir (si il y a plus de 3500 habitants).

Les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne. (art. L. 262 du code électoral)

.. figure:: sieges_elu.png

   Répartition des sièges

Envoi préfecture
----------------

C'est une action du formulaire élection qui fabrique un fichier en format csv respectant la normalisation préfectorale.

Vérifiez que tous les paramètres suivants sont renseignés :

- code prefecture des candidats (sous formulaire Candidat(s) de l'élection)

- code canton et circonscription des unites (formulaire unites)

- département et commune des unites (formulaire unites)

- code prefecture du type élection
 
La case à cocher initial (du formulaire election) permet d'informer la prefecture si il s'agit d'un fichier initial ou non.

Edition et extraction
---------------------

Différente éditions sont mises à la disposition de l'utilisateur.
Pour les utiliser, il suffit de cliquer sur l'action portant le nom de l'édition.

Il est également possible de récupérer les résultats sous la forme d'un fichier csv à l'aide de l'action *extraction*.
