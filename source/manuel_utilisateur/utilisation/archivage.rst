.. _archivage:

Archivage
=========

La dernière étape de l'élection c'est l'archivage.
Pour archiver une élection, il faut être à l'étape de finalisation et cliquer sur l'action *Aller à l'étape \*archivage\** dans le portlet de l'élection.

.. note::
   Les élections archivées ont une couleur différente dans le listing des élections.
   De plus, elles ne sont pas affichées sur le widget **Élections**.

Durant l'étape d'archivage, le paramétrage de l'élection ne peut plus être modifié. Il n'est plus ainsi plus possible de modifier l'élection, d'ajouter/modifier ou supprimer des candidats, des centaines, des plans et/ou des animations. Les résultats et la participation ne peuvent également plus être saisis ni publiés ou dépubliés.

Il est cependant possible de consulter le portail web, ainsi que les animations et les éditions. L'extraction des résultats et le transfert en préfecture sont encore possible.

.. figure:: portlet_election_archivee.png

   Actions disponibles

Les résultats enregistrés pour l'élection sont également facilement consultable depuis le tableau de bord.

.. figure:: tb_archivage.png

   Tableau de bord de l'étape archivage
