.. _parametrage_utilisation:

Étape de paramétrage
====================

La première étape du workflow de l'élection est l'étape de paramétrage.
Au cours de cette étape, l'utilisateur doit configurer l'élection (cf. partie élection) mais également ajouter les candidats, ajouter les centaines, créer les animations et ajouter les plans.
Il peut également importer les inscrits au cours de cette étape.

Durant cette étape, seules les actions relatives au paramétrage sont utilisables.
Il est également possible de modifier la configuration de l'élection : nom, périmètre, options, etc.

.. note::

   La modification du périmètre ou des horaires entrainera la mise à jour des unités et des tranches horaires de l'élection.

.. figure:: portlet_parametrage.png

   Actions disponibles


Ajout des candidats
-------------------

L'ajout des candidats se fait à partir de l'onglet *candidat(s)* de l'élection.
Au cours, de l'étape de paramétrage, il est possible d'ajouter/supprimer/modifier des candidats.

Pour ajouter un candidat, cliquer sur le bouton d'ajout du formulaire et remplir le formulaire d'ajout.

Les champs du formulaire sont les suivants :

- election candidat : identifiant du candidat pour cette élection qui lui est attribué lors de son ajout à l'élection (pré-rempli)
- election : identifiant de l'élection (pré-rempli)
- candidat : choix du candidat parmis la liste des candidats paramétrés précedemment (voir :ref:`Candidats '<parametrage_candidat>`)
- ordre : numéro d'ordre du candidat pour affichage
- code : code préfecture du candidat
- age moyen : âge moyen pour la liste municipale
- siège : nombre de sièges obtenus au conseil municipale (pas saisissable à cette étape)
- age moyen com : âge moyen pour la liste communautaire
- siège com : nombre de sièges obtenus au conseil communautaire (pas saisissable à cette étape)
- age moyen mep : âge moyen pour la liste métropolitaine
- siège mep : nombre de sièges obtenus au conseil méropolitain (pas saisissable à cette étape)
- couleur : couleur attribuée au candidat dans les animations
- photo : photo du candidat pour les animations

.. figure:: form_candidat.png

   Formulaire d'ajout des candidats

Création des centaines
----------------------

Les centaines sont créées depuis l'onglet *centaine(s)* de l'élection.

Pour créer une centaine, il faut cliquer sur le bouton d'ajout et remplir les champs suivant :

- libellé : nom de la centaine
- votant : nombre de votant de la centaine (par défaut 100)

.. figure:: form_centaine.png

   Formulaire de création des centaines

Une fois créée, les résultats par unités de la centaine sont visibles, dans la page de consultation de cette centaine.

.. figure:: tab_centaine.png

   Tableau de bord des centaines

Création des animations
-----------------------

Il est possible de lister les animations pour l'élection dans le sous-formulaire *animation(s)* de l'élection.

Il est possible de modifier/supprimer une animation, en la sélectionnant dans la liste et en appuyant sur *modifier* ou *supprimer*.
Il est également possible de la visualiser en cliquant sur *animation* ou de l'activer/la désactiver en cliquant sur *activer/desactiver*.
Cependant, a cette étape du workflow aucun résultat n'a été envoyé la visualisation n'affichera donc rien pour l'instant à part l'entête et le titre.

Le formulaire se remplit comme celui des animations du paramétrage métier à l'exception des champs *élection*, *périmètre à afficher*, *type des unités à afficher* et *élections de comparaison* qui sont maintenant paramétrables.

.. warning::
   Lors de l'ajout de l'animation, il est possible de choisir si l'animation concerne l'élection ou une de ses centaines.
   Après validation, ce choix ne sera plus modifiable.

.. note::
   Le choix d'un périmètre va avoir un impact sur les unités affichées.
   En effet, seules les unités appartenant à ce périmètre seront visibles à l'affichage.
   De la même manière, le choix du type va déterminer quelles unités, appartenant au perimètre, seront affichées.
   
   Exemple : Si l'élection porte sur une ville avec deux mairies. La première mairie possède 4 bureaux de votes et la deuxième 1 seul.
   En choisissant comme périmètre la ville et comme type d'unité la mairie, les unités visibles à l'affichage seront les deux mairies.
   Si le type d'unité choisi est le bureau de vote alors les 5 bureaux seront visibles dans l'animation.
   Par contre, si la deuxième mairie est choisi comme périmètre, en choisissant comme type le bureau de vote, seul son bureau sera visible.

.. note::
   L'élection de comparaison n'aura un impact que pour l'affichage de la comparaison de la participation.
   Sur cette animation seul les élections dont la participation a été transmise à l'animation seront visible.

.. figure:: form_animation.png

   Formulaire de l'animation

Ajout des plans
---------------

L'ajout des plans se fait depuis l'onglet *plan(s)* de l'élection.

Pour ajouter un plan, cliquer sur le bouton d'ajout et sélectionner le plan à ajouter dans le champ plan.

.. figure:: form_plan_election.png

   Formulaire d'ajout des plans

.. note::

   Lors de l'ajout des plans, il est possible de choisir si ce plan doit être visible sur le portail web de l'élection ou d'une de ses centaines.

   Lors de l'ajout le paramétrage des plans est enregistré dans le répertoire du portail web pour affichage.
   Ce paramétrage est indépendant de celui du plan paramétré pour l'élection. Ainsi, en modifiant le paramétrage
   du plan ou le positionnement des unités depuis l'application, les modifications ne seront pas visibles
   lors de l'affichage du plan.
   
   Cette dissociation a pour but de conserver le paramétrage des plans de l'élection même si le paramétrage global évolue.
   
   Pour mettre à jour le paramétrage de tous les plans, utiliser l'action *Mettre à jour le paramétrage des plans* du portlet d'un plan.

Délégation de saisie
--------------------

Le paramétrage de la délégation de saisie se fait depuis l'onglet *délégation* de l'élection.

La délégation de saisie est très simple à paramétrer. Il suffit de choisir un acteur et de séléctionner l'unité pour laquelle il aura le droit de saisir des résultats.

Il est également possible de paramétrer plusieurs délégation à la fois via le formuaire de délégation multiple.

.. figure:: form_delegation_multiple.png

   Formulaire de la délégation multiple

.. note::
   Il n'y a pas besoin de paramétrer les acteurs pour la délégation de saisie. Ils sont automatiquement créés lors de l'ajout des utilisateurs et supprimés si les utilisateurs correspondant le sont.

   Cependant, il est quand même possible de les paramétrer si besoin depuis le paramétrage métier.

Import des inscrits
-------------------

Il est possible d'importer le nombre d'inscrit à partir des csv issus du REU.

Pour cela, cliquer sur l'action *importer les inscrits* du portlet de l'élection.
Le formulaire suivant va s'afficher.

.. figure:: form_import_inscrit.png

   Formulaire d'import des inscrits

Dans ce formulaire, il y a trois champs à saisir :

- Fichier CSV : sert à choisir le fichier pour l'import
- Séparateur : séparateur dans le csv (',' ou ';')
- Liste d'inscrit à importer

Une fois ces champs saisis, cliquer sur *Importer* et les inscrits seront importer.

.. warning::
   L'import se fait en utilisant l'identifiant de l'unité issue du REU.
   L'import des inscrits ne concerne donc que les unités importer (voir :ref:`Import des unités'<import_unite>`)

Vérification du paramétrage
---------------------------

Pour permettre à l'utilisateur de facilement valider son paramétrage plusieurs solutions ont été mise en place.

Premièrement, les éléments paramétrés sont notés dans le tableau de bord de l'élection, visible dans l'onglet *election*.

.. figure:: tb_parametrage.png

   Tableau de bord de l'étape paramétrage

Deuxièmement, l'utilisateur peut utiliser l'action *verification parametrage*.
Cette action affiche un message recensant les différents éléments paramétrés et précisant si le paramétrage est correct.

Troisièmement, lors du passage à l'étape suivante, une vérification est automatiquement effectuée permettant à l'utilisateur de voir immédiatement si son paramétrage n'est pas le bon.
