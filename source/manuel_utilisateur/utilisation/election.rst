.. _election:

Configuration de l'élection
===========================

Pour commencer, il faut configurer l'élection. La liste des élections est accessible 
dans le menu  **Application -> Election**

.. figure:: tab_election.png

   Listing des élections

Saisie élection
---------------

Le formulaire de saisie de l'élection est découpé en spet sections :

1. Election : champs relatifs aux informations de l'élection.
   
   - libellé : nom de l'élection
   - code : code de l'élection
   - tour : 1er ou 2ème tour
   - type : type de l'élection, ex : municipale
   - date : date à laquelle à lieu l'élection

2. Perimetre : champ permettant de choisir le périmètre de l'élection.
   Le choix du périmètre est obligatoire car les unités associées à l'élection dépendent du périmètre choisi.

3. Participation : sélection de l'heure d'ouverture et de fermeture des unités de saisie. Les heures d'ouverture et de fermeture peuvent être positionnées par défaut via un paramètre modifiable par l'administrateur (voir :ref:`Paramètres généraux '<administration_parametre>`).
   Le choix des horaires est obligatoire meme si la participation n'a pas besoin d'être saisie.

4. Préfecture : permet d'indiquer lors de l'envoi des résultats en préfecture si il s'agit d'un envoi initial ou pas.

5. Workflow : non modifiable. Indique l'étape d'avancement de l'élection.

6. Paramétrage : choix des options de configuration de l'élection. Les options sont les suivantes :
   
   - Publier automatiquement les résultats : permet de publier les résultats automatiquement après validation
     du formulaire de saisie.
   - Publier les résultats en défaut : Utile uniquement si la publication automatique est active.
     Si non sélectionné, les résultats présentant des erreurs de saisie ne seront pas automatiquement publier. 
   - Calcul automatique des votes exprimés : lorsque cette case est cochée, le nombre de votes exprimés n'est
     plus saisissable par l'utilisateur et il est automatiquement calculé lors de l'enregistrement des résultats,
     à la place.
   - Garder les résultats après la simulation : permet de garder les résultats saisis pendant l'étape de simulation
     lors du passage à l'étape de saisie.
   - Déléguer la saisie : met en place la délégation de saisie pour les résultats et permet d'accéder au formulaire
     de délégation de saisie
   - Déléguer la saisie de la participation : permet de mettre en place la délégation de saisie de la participation.
     Ne fonctionne que si la délégation de saisie est active.
   - Validation obligatoire pour la publication : rend la validation des résultats obligatoire avant de pouvoir les
     envoyer à l'animation et au portail web.

7. Répartition : Nombre de sièges pour les conseils : municipal, communautaire et métropolitain.

.. figure:: form_election.png

   Formulaire de l'élection

Création de l'élection
----------------------

Lors de la création de l'élection (après avoir validé le formulaire) plusieurs éléments sont automatiquement paramétrés.

Pour commencer, le périmètre et toutes les unités qu'il contiens sont automatiquement affectés à l'élection.

Ensuite, les tranches horaires sont automatiquement affectées à l'élection.

.. note::

   L'ajout des tranches horaires est réalisé en sélectionnant toutes les tranches dont le numéro
   d'ordre est compris entre celui de l'heure d'ouverture et celui de l'heure de fermeture.

Les plans pour lesquels la case *par défaut* a été cochée sont également ajoutés à l'élection.

Les sous formulaires d'élection
--------------------------------

Les sous formulaires sont les suivants :

- candidat(s) : ajouts et paramétrage des candidats de l'élection

- unité(s) : saisie des résultats et centaines

- participation(s) : saisie de la participation

- centaine(s) : création des centaines

- animation(s) : paramétrage et affichage des animations pour l'élection et ses centaines

- plan(s) : paramétrage des plans pour l'élection et les centaines

- délégation : si l'option de délégation est active, paramétrage de la délégation de saisie
