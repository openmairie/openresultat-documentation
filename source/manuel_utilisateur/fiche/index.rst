.. _fiche:

######################
Exemple d'utilisation
######################

Le but de ce chapitre est de montrer comment fonctionne l'application à travers un exemple.

L'élection présentée, dans cet exemple, est une élection municipale se déroulant le 09/06/2021.
L'élection est réalisée pour une commune qui possède six bureaux de vote. Les bureaux de vote ouvrent à 8h et ferme à 18h. Deux candidats se présentent à cette élection.
Pour cette election, le plan de la vile avec les unités positionnées dessus est également à paramétrer.

Les types d'élection, types d'unités et tranches horaires ainsi que le modèle web ont déjà été paramétrés.

Les éléments à paramétrer pour cet exemple sont donc :

- les candidats
- les unités
- l'élection


Paramétrage métier
==================

1. Paramétrage des unités

La première étape consiste à créer les unités de saisie. Il y en a sept à paramétrer : les six bureaux de vote et la commune.

.. figure:: parametrage_unite.png

   Paramétrage du premier bureaux de vote

Le paramétrage de la commune est un peu différent. En effet, la commune doit contenir les bureaux de vote, c'est donc un **périmètre**.
Pour paramétrer la commune, et pouvoir l'associer aux bureaux par la suite, il faut remplir le champ *type voulu* en choisissant le type *bureau de vote* dans la liste (type associé aux unités précédemment créées).
Pour indiquer qu'une unité est un périmètre la case périmètre doit être cochée. Cette case est automatiquement cochée lorsqu'un type voulu est séléctionné.

.. figure:: parametrage_perimetre.png

   Paramétrage du périmètre

Une fois les unités créées, il reste à les lier. Pour cela, il faut se rendre dans le listing des unités.
Cliquer sur la commune, puis sur l'onglet *Lien entre les unités* et enfin sur l'icone à droite de la croix verte.
Aprés avoir cliqué, le formulaire d'ajout des liens s'affichent. Dans ce formulaire, le premier champs (le périmètre) est présélectionné.
Ensuite dans le second champ, il faut choisir parmis la liste les unités à lier à ce périmètre, c'est à dire les 6 bureaux de vote.

.. figure:: parametrage_lien.png

   Lien entre la commune et les bureaux

Il suffit ensuite de cliquer sur ajouter pour que les liens soient créés.

1. Paramétrage des candidats

Il faut ensuite créer les candidats de l'élection. La création des candidats se fait via le menu **Administration & Paramétrage -> candidat**.
Il faut ensuite cliquer sur la croix verte du listing pour ajouter un nouveau candidat.

Le paramétrage des candidats est très simple. Il consiste uniquement à saisir le nom du candidat et le nom de la liste (facultatif) auquelle il appartient.

.. figure:: parametrage_candidat.png

   Formulaire des candidats

Il reste ensuite à répéter l'opération pour chaque candidat à paramétrer.

3. Paramétrage du plan

Pour créer le plan, il faut se rendre dans le menu le menu **Administration & Paramétrage** et cliquer sur plan, puis sur le bouton d'ajout.
Pour paramétrer le plan, il faut lui donner un libellé, choisir une image du plan et des images pour représenter les unités arrivées et non arrivées.

Dans cet exemple, la case par défaut est également cochée pour que le plan soit immédiatement associé à l'élection lors de sa création.

.. figure:: parametrage_plan.png

   Paramétrage du plan

Il faut ensuite associer les bureaux de vote au plan. Pour cela, cliquer sur l'onglet *plan unite* et cliquer sur le bouton d'ajout.
Dans le formulaire d'ajout, commencer par sélectionner le plan voulu puis l'unité à placer.
Il est ensuite possible de choisir les icônes qui représenteront l'unité sur le plan.
Dans cet exemple, on se contente d'utiliser les icônes par défaut, ces champs ne sont donc pas remplis.

Pour finir, il faut placer le bureau sur le plan. Pour cela, cliquer sur l'icone du champ prévisualisation.
La fenêtre suivante s'ouvre :

.. figure:: previsualisation.png

   Positionnement du bureau sur le plan

Il faut ensuite cliquer sur l'unité et la déplacer à l'endroit voulu puis double cliquer dessus. La fenêtre va se fermer et vous constaterez que les coordonnées de l'unité ont été remplies dans le formulaire.

.. figure:: parametrage_plan_unite.png

   Paramétrage du bureau sur le plan

Répéter cette étape pour chaque unité à afficher sur le plan.

Configuration de l'élection
===========================

Une fois le paramétrage terminé, il faut configurer l'élection.
Pour cela accéder au tableau de bord et aller dans **Application -> election** avant de cliquer sur le bouton d'ajout.

La configuration de l'élection est la suivante :

.. figure:: configuration_election.png

   Configuration de l'élection

Le périmètre choisi est la commune précedemment paramétrée et comme présenté plus haut l'heure d'ouverture est *08:00:00* et la fermeture est à *18:00:00*.
Pour cette élection, on a choisit d'avoir le calcul automatique des votes exprimés et la publication automatique des résultats.
Une fois la configuration terminée, soumettre le formulaire pour créer l'élection.

Etape de paramétrage
====================

La première étape de préparation de l'élection est l'étape de paramétrage. L'étape au cours de laquelle, il faut ajouter les candidats, créer les centaines, mettre en place les animations et ajouter les plans.

Lors de la création de l'élection, les unités, les tranches horaires et les plans par défaut sont automatiquement ajoutés à l'élection.
Notre plan étant un plan par défaut, il n'y a donc pas besoin de l'ajouter à l'élection car il a été ajouté lors de sa création.

1. Ajout des candidats

Pour ajouter des candidats se rendre dans l'onglet *candidat(s)* de l'élection et cliquer sur le bouton d'ajout.

Dans le formulaire, sélectionner dans le champ candidat *hervé SCHIAVETTI* et saisir son code préfecture. Le numéro d'ordre est pré-rempli et n'a pas besoin d'être modifié.
Remplir également l'âge moyen pour les listes municipales et communautaires dans le menu *Listes Municipales* du formulaire.
Pour finir sélectionner la couleur associée au candidat dans les animations et éventuellement une photo avant de valider le formulaire.

.. figure:: ajout_candidat.png

   Formulaire d'ajout du premier candidat

Faire de même avec le second candidat.

2. Création d'une centaine

Pour cet élection, on souhaite saisir la première centaine. On va donc créer une centaine dans l'onglet *centaine(s)*.

Dans le formulaire d'ajout, commencer par nommer la centaine. Ensuite, on choisit le nombre de votant, comme c'est une centaine on choisit 100.

.. figure:: creation_centaine.png

   Paramétrage de la centaine

Valider le formulaire pour ajouter la centaine.

3. Création des animations

On souhaite avoir trois animations. Une pour les résultats, une pour la participation et une pour le nombre de sièges obtenus par candidat.

Les animations sont créées dans l'onglet *animation(s)*. En cliquant sur le bouton d'ajout, on accéde au formulaire d'ajout d'une animation.

Dans ce formulaire, commencer par choisir l'élection et saisir le nom de l'animation.
Pour simplifier le paramétrage de l'animation, on se sert d'un modèle.
Dans le champ modèle choisir le modèle voulu (Résultat, Participation ou Liste municipale).
Une fois le modèle choisit le paramétrage du modèle est copié dans le formulaire.
Il ne reste plus qu'à choisir le périmètre d'affichage des résultats (*COMMUNE*) et le type d'unité que l'on souhaite afficher (*Bureau de vote*).

.. figure:: creation_animation.png

   Paramétrage de la première animation

Répéter le processus pour les deux animations restantes.

Etape de simulation
===================

Le paramétrage étant terminé, on passe à l'étape de simulation en cliquant sur l'action *aller à l'étape \*simulation\** du portlet de l'élection.
L'étape de simulation est l'étape durant laquelle on peut tester l'élection. Cette étape ne nous intéresse pas dans cet exemple, on passe donc à l'étape suivant en cliquant sur l'action *aller à l'étape \*saisie\**

Etape de saisie
===============

1. Saisie des résultats de la centaine

On commence par la saisie des résultats de la centaine. Pour cela, il faut se rendre dans l'onglet
*unité(s)*. Ensuite, cliquer sur l'unité dont on souhaite remplir les résultats, dans cet exemple c'est l'unité *1 Salle des Mariages*.

Dans un premier temps, saisir le nombre d'inscrit de l'unité. Pour cela, il faut accéder au formulaire de saisie
de l'unité en cliquant sur l'action *saisir les résultats*. Ensuite remplir uniquement le nombre d'inscrit et valider.

Dans le portlet d'action de l'unité, une des actions porte le nom de la centaine : *Municipales 2019 1er tour 1ère centaine*.
En cliquant, sur cette action, on accéde au formulaire de saisie des résultats. Il suffit ensuite d'entrer les résultats
et de valider le formulaire.

.. figure:: formulaire_saisie_centaine.png

   Saisie des résultats de la centaine

Faire de même pour chacune des unités, sauf le périmètre dont les résultats sont automatiquement calculés.

Pour vérifier si les résultats sont corrects, se rendre ensuite dans l'onglet *centaine(s)* et cliquer
sur la centaine pour voir le tableau récapitulatif des résultats.

.. figure:: tableau_saisie_centaine.png

   Tableau de bord de la centaine

2. Saisie des résultats

On viens maintenant saisir les résultats. Pour cela cliquer sur l'onglet *unite(s)* et choisir l'unité
à saisir. Ensuite dans le portlet, cliquer sur *saisir les résultats*.
On accède ainsi au formulaire de saisie dans lequel on va pouvoir saisir les résultats.

.. figure:: formulaire_saisie_resultats.png

   Saisie des résultats

Répeter cette étape pour chaque bureau de vote. Les résultats de la commune seront automatiquement mis
à jour au fur et à mesure de la saisie des résultats des bureaux de vote.

3. Saisie de la participation

Pour finir, saisir la participation. Pour cela, se rendre dans l'onglet *participation(s)* et choisir
la tranche horaire à saisir. Ensuite, dans le portlet cliquer sur *Saisir la participation* pour accéder
au formulaire de saisie.

Dans ce formulaire, saisir la participation enregistrée pour chaque bureau à l'heure choisi. La participation
de la commune va automatiquement se calculer au cours de la saisie.

.. figure:: formulaire_saisie_participation.png

   Saisie de la participation

Répéter l'opération pour chaque tranche horaire.

Finalisation
============

La saisie étant terminée, on passe à l'étape de finalisation en cliquant sur *aller à l'étape \*finalisation\**, dans le portlet de l'élection.

1. Calcul des sièges

Puisque tous les résultats ont été saisis, on souhaite maintenant connaître le nombre de sièges
obtenus par chaque candidat.
Pour cela, cliquer sur l'action *calcul de siège élu* du portlet de l'élection.

En cliquant, sur cette action le message suivant est affiché et le nombre de sièges obtenus
par chaque candidat est enregistré.

.. figure:: sieges_elu.png

   Calcul des sièges

2. Visualisation des animations

Avant de visualiser les animations, il faut envoyer les informations nécessaires aux animations.
Pour cela, se rendre dans l'onglet *participation(s)*, accéder à une tranche horaire et cliquer
sur *affichage tranche*. Répéter cette opération pour chaque tranche horaire.

.. note::

   Dans cet exemple, il n'y a pas besoin de publier les résultats car l'option de publication
   automatique a été sélectionnée. Si jamais cette option n'a pas été sélectionnée, il vous faut
   pour chaque unité accéder à l'unité et cliquer sur l'action *envoi à l'animation*.

Pour visualiser les animations, il faut se rendre dans l'onglet *animation(s)* et sélectionner
l'animation à visualiser. Les animations sont les suivantes :

.. figure:: affichage_liste.png

   Animation de la répartition des sièges

.. figure:: affichage_resultats.png

   Animation des résultats

.. figure:: affichage_participation.png

   Animation de la participation

3. Portail web

Avant d'accéder au portail web, il faut envoyer les informations nécessaires au répertoire web de l'élection.
Pour cela, se rendre dans l'onglet *participation(s)*, accéder à une tranche horaire et cliquer
sur *afficher web*. Répéter cette opération pour chaque tranche horaire.

.. note::

   Dans cet exemple, il n'y a pas besoin de publier les résultats car l'option de publication
   automatique a été sélectionnée. Si jamais cette option n'a pas été sélectionnée, il vous faut
   pour chaque unité accéder à l'unité et cliquer sur l'action *envoi au portail web*.

L'accès au portail web se fait depuis le tableau de bord en cliquant sur le widget *Portail Web*.

Sur le portail web commencer par cliquer sur l'élection voulue.
La participation est ensuite consultable depuis l'onglet *participation*.

.. figure:: participation_web.png

   Affichage de la participation sur le portail web

Le résultat global de l'élection est visible dans l'onglet *résultats*.

.. figure:: resultats_web.png

   Affichage des résultats sur le portail web

On peut également visualiser le plan précédemment paramétré en cliquant sur l'onglet portant
le nom du plan.

.. figure:: plan_web.png

   Affichage du plan sur le portail web

Ensuite, en cliquant sur une des unités on accède à ses résultats.

.. figure:: affichage_web_u1.png

   Détail des résultats d'une unité

4. Proclamation des résultats et envoi à la préfecture

Pour finir, on souhaite accéder aux éditions de l'élection. Ces éditions sont accessibles depuis
l'onglet *election*. Il en existe plusieurs mais on va s'intéresser ici à la proclamation des
résultats.

Pour accéder à cette édition, cliquer sur l'action correspondante : *proclamation resultat*.

Les résultats validés, on souhaite les transmettre à la préfecture. La transmission à la préfecture
se fait en utilisant un fichier csv.

Il s'agit d'un envoi initial à la préfecture, il faut donc l'indiquer en cliquant sur l'action *modifier*
de l'élection et en cochant la case **envoi initial** avant de valider le formulaire.

Ensuite, il ne reste plus qu'à cliquer sur l'action *Envoi à la préfecture* et à télécharger le fichier
csv.

Archivage
=========

Une fois, l'étape de finalisation achevée passer à l'étape d'archivage en cliquant sur l'action 
*Archiver l'élection*. Notre élection est maintenant terminée !
