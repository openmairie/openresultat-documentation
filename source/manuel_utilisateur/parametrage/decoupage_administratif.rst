.. _decoupage_administratif:

Le découpage administratif regroupe les cantons, les circonscriptions, les communes et les départements.
Ces quatres éléments servent à récupérer toutes les informations nécessaires, à un bureau 
de vote, lors de l'envoi des résultats à la préfecture et pour les éditions. Ils sont donc liés aux unités et
servent à récupérer les différents codes (code commune, code département, etc.) qui permettent
de caractériser une unité.

Ils ont ainsi été rassemblés dans la partie *DÉCOUPAGE ADMINISTRATIF*.


Canton
======

Il est possible de lister les cantons dans la rubrique *canton*.

.. figure:: tab_canton.png

   Listing des cantons

Il est possible de modifier / supprimer les cantons dans le formulaire de saisie des cantons
en appuyant sur modifier ou supprimer

.. figure:: form_canton.png

   Formulaire d'un canton

Les champs suivants peuvent être mis a jour :

Le champ *'libellé'* est un champ libellé.

Le champ *'code'* correspond au code du canton.

Le champ *'préfecture'* permet de renseigner le code donné par la préfecture à ce canton.

Circonscription
===============

Il est possible de lister les circonscriptions dans la rubrique *circonscription*.

.. figure:: tab_circonscription.png

   Listing des circonscriptions

Il est possible de modifier / supprimer les circonscriptions dans le formulaire de saisie des circonscriptions
en appuyant sur modifier ou supprimer

.. figure:: form_circonscription.png

   Formulaire d'une circonscription

Les champs suivants peuvent être mis a jour :

Le champ *'libellé'* est un champ libelle.

Le champ *'code'* correspond au code de la circonscription.

Le champ *'préfecture'* permet de renseigner le code donné par la préfecture à cette circonscription.

Commune
========

Il est possible de lister les communes dans la rubrique *commune*.

.. figure:: tab_commune.png

   Listing des communes

Il est possible de modifier / supprimer les communes dans le formulaire de saisie des communes
en appuyant sur modifier ou supprimer

.. figure:: form_commune.png

   Formulaire d'une commune

Les champs suivants peuvent être mis a jour :

Le champ *'libellé'* est un champ libellé.

Le champ *'code'* correspond au code de la commune.

Le champ *'préfecture'* permet de renseigner le code donné par la préfecture à cette commune.

Département
===========

Il est possible de lister les départements dans la rubrique *département*.

.. figure:: tab_departement.png

   Listing des départements

Il est possible de modifier / supprimer les départements dans le formulaire de saisie des départements
en appuyant sur modifier ou supprimer

.. figure:: form_departement.png

   Formulaire d'un département

Les champs suivants peuvent être mis a jour :

Le champ *'libellé'* est un champ libellé.

Le champ *'code'* correspond au code du département.

Le champ *'prefecture'* permet de renseigner le code donné par la préfecture à ce département.
