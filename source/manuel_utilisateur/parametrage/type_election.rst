.. _type_election:


Saisie type_election
====================

L'accès au type d'élection se fait dans la rubrique *type d'élection*.

Le formulaire d’un type d’élection est composé de quatre champs :

- Id : identifiant du type d’élection utilisé lors de la création d’une élection. Il est automatiquement choisit lors de l'ajout du type d'élection et ne peut pas être modifié.

- Libellé : nom donné aux types d'élection pour les identifier.

- Code : champ de codification interne pour identifier le type

- Prefecture : code du type d’élection réglementaire (CAN, DEP, EUR, LEG, MUN, PRE, REF, REG) utilisé pour la transmission des résultats en préfecture.

.. figure:: form_type_election.png

   Formulaire du type d'élection

La liste des types d’élection est sur quatre colonnes pour afficher tous les champs.

.. figure:: tab_type_election.png

   Listing des types d'élection
