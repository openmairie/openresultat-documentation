.. _unite:

Les unités de saisie permettent d'avoir plus de souplesse dans la prise en charge des différents types d'élection. Elles permettent de :

   - structurer le périmètre des élections
   - faciliter le paramétrage des unités pour les élections
   - réaliser la saisie des résultats à différents niveaux (bureaux, communes, cantons, etc.)
   - réaliser des affichages selon le types d'unités


Le paramétrage des unités se fait en trois partie : la définition des types d'unités, le paramétrage des unités et la création de liens entre ces unités.


Type d'unité
============

Le paramétrage des types d'unité se fait dans la rubrique *Type d'unité*.

La liste des types d'unité paramétrés est accessible depuis cet onglet. Il est possible d'ajouter ou de consulter les types d'unités créés depuis ce listing.

.. figure:: tab_type_unite.png

   Listing des types d'unité

Le formulaire d'ajout/modification des types d'unités est composé de trois champs :

   - libelle : nom donné aux types d'unité pour les identifier facilement
   - hiérarchie : défini l'ordre de grandeur des types d'unité pour faciliter la structuration des périmètres.
     Le principe est que les unités peuvent contenir uniquement des types d'unité dont la hiérarchie est inférieure
     à la leur. Par exemple : une mairie de hiérarchie 3 ne pourra pas contenir une région de hiérarchie 5.
     En revanche, elle pourra contenir un bureau de vote de hiérarchie 1. Pour éviter que la modification de
     cette valeur ne viennent compromettre la hierarchie déjà mise en place, une vérification est effectuée.
     Ainsi en modification, si une valeur de hiérarchie, non correcte, est saisie, un message viendra avertir
     l'utilisateur.
   - bureau de vote : indique si une unité doit se comporter comme un bureau de vote. Ce comportement aura un
     impact sur le calcul du nombre de siège, l'affichage du portail web, certaines éditions et autres.
     Il est donc important de bien choisir quel type d'unité se comportera comme un bureau de vote.

.. figure:: form_type_unite.png

   Formulaire du type d'unité

Saisie unite
=============

Le paramétrage des unités se fait depuis la rubrique *unité*.

La liste des unités paramétrées est accessible depuis cette rubrique. Il est possible d'ajouter ou de consulter les unités créées depuis ce listing.

.. figure:: tab_unite.png

   Listing des unités


Le formulaire d'ajout/modification des unités est composé des champs suivant :

   - libellé : nom donné à l'unité.
   - type : type d'unité parmis les types précédemment paramétrés.
   - ordre : code de l'unité de saisie.
   - adresse : La saisie de l'adresse se fait par recherche en autocomplete sur le champ adresse1 en tapant les premiers caractères de l'adresse (numéro, type de voie, nom de la voie). La recherche est faite avec l'API de la BAN (base d'adresse nationale) et il est retourné le CP, la ville et les coordonnées du point d'adresse.
   - complément d'adresse : complément d'adresse si besoin
   - cp : code postal, fonctionne aussi en autocomplete et la ville est recherchée quand le code postal est validé.
   - ville : ville, fonctionne aussi en autocomplete. La recherche du code postale est faite quand la ville est séléctionnée. Si le code postal ou la ville sont complétés, la recherche de l'adresse se fait sur la ville ou le code postal concerné.
   - périmètre : case à cocher si l'unité est un périmètre, c'est à dire si elle contiens d'autres unités
   - type voulu : type d'unité devant être contenu par l'unité. Si un type d'unité voulu est séléctionné la case périmètre est automatiquement cochée.
   - début de validité : date de début de validité de l'unité de saisie
   - fin de validité : date de fin de validité de l'unité de saisie. Les unités, dont la date de fin de validité est dépassée, ne sont plus affichées, dans la liste des unités. En revanche, elle seront toujours disponibles sur l'application en cliquant sur **Afficher les elements expirés**
   - code unité : code de l'unité (uniquement pour un bureau de vote)
   - canton : choix du canton auquel appartient l'unité (uniquement pour un bureau de vote)
   - département : choix du département auquel appartient l'unité (uniquement pour un bureau de vote)
   - circonscription : choix de la cicrconscription auquelle appartient l'unité (uniquement pour un bureau de vote)
   - commune : choix de la commune auquelle appartient l'unité (uniquement pour un bureau de vote)

.. figure:: form_unite.png

   Formulaire d'une unité

.. _import_unite:

Import des unités
=================

Il est également possible d'importer les unités de saisie à l'aide d'un fichier csv issus d'Elire.
L'accès au formulaire d'import se fait depuis le listing des unités en cliquant sur l'icône : |icone_import|.

Le formulaire d'import est composé de deux champs :

   - Fichier CSV : permet de sélectionner le fichier csv
   - Séparateur : caractère servant de séparateur dans le csv

Sous le formulaire se trouve des informations concernant les champs du fichier csv (obligatoire ou pas, type de champ, etc.).
Il est également possible de télécharger un csv modèle pour voir comment doivent être organisées les informations du csv servant à l'import.

.. figure:: form_import.png

   Formulaire d'import des unités

Une fois le formulaire rempli, cliquer sur importer pour réaliser l'import des unités de saisie.

.. warning::
   Les unités de saisie issues d'Elire sont des bureaux de vote.
   Afin que les unités importées aient le type voulu, il est important de paramétrer le type d'unité (voir :ref:`Paramètres généraux '<administration_parametre>`).
   Sinon l'import ne peut pas se faire.

.. note::
   Lors de l'import si les colonnes "canton" et "circonscription legislative" du fichier csv sont remplies, les cantons et circonscriptions seront automatiquement ajoutés à la base de données, si ils n'existent pas déjà.

Lien entre les unités
=====================

Le lien entre les unités sert à structurer les périmètres. Cette structure est composée d'unités (les périmètres ou unités parents) qui contiennent d'autres unités (les unités enfants).
Cette structure possède deux contraintes :

1. Elle doit respecter la hiérarchie entre les types d'unité.
2. Une unité ne peut contenir qu'un seul type d'unité.

Ainsi, supposons que l'on souhaite réaliser une élection au niveau d'une mairie (A) et que cette mairie est en charge de deux bureaux de vote (B et C).
La mairie (A) est donc une unité périmètre qui va contenir les unités de type bureaux de vote (B et C). Le lien entre ces unités est donc que la mairie (A) est l'unité parent
et les bureaux de vote (B et C) sont ses unités enfants. Une mairie peut contenir des bureaux de vote, la hiérarchie est donc respectée. De plus, il n'y a que des bureaux qui sont contenus,
la contrainte du type unique est également respectée. En liant ces unités ensemble, on obtiens la structure de l'élection.

Pour lié une unité à d'autre, il faut sélectionner une unité puis cliquer sur l'onglet *Lien entre les unités*.
Il est possible de créer ou de consulter les liens de l'unité choisie depuis ce listing.

.. figure:: tab_lien_unite.png

   Liste des liaisons d'une unité

Le formulaire de création/modification des liens entre les unités est très simple. Il se compose de deux champs :

   - unité parent : l'unité qui va contenir l'autre unité. Seuls les périmètres peuvent être choisis. (Pré-rempli avec l'unité choisie)
   - unité contenu : unité contenue par l'unité parent. Seules les unités, dont le type correspond au type recherché par l'unité parent, peuvent être sélectionnées.

.. figure:: form_lien_unite.png

   Formulaire de liaison des unités

Il est également possible de créer plusieurs lien d'un coup. Pour cela il faut cliquer sur l'icone : |lien_multiple|.

.. figure:: form_lien_unite_multiple.png

   Formulaire de liaison des unités

.. Raccourcis pour avoir des images affichées en ligne

.. |icone_import| image:: icone_import.png
.. |lien_multiple| image:: icone_lien_multiple.png