.. _animation:


Saisie animation
================

La liste des animations est visible dans la rubrique *animation*.

.. figure:: tab_animation.png

   Listing des modèles d'animation

Depuis ce listing, il est possible d'ajouter/consulter des modèles d'animation.
Ces animations pourront servir de modèle pour les animations des élections.

Paramétrage des animations
==========================

Le formulaire des animations est le suivant :

.. figure:: form_animation.png

   Formulaire d'une animation

Les champs paramétrables sont :

- *'libellé'* : libellé des modèles d'animation qui permettra de les identifier.

- *'refresh'* : intervalle de temps, en seconde, entre deux raffraichissements de l'animation.

- *'garder comme modèle'* : case indiquant si l'animation est un modèle ou pas. Non paramétrable.

- *'actif'* : case indiquant si le modèle est actif ou pas. Seul les modèles actif pourront être utilisés comme modèle.

- *'titre'* : titre affiché dans l'entête de l'animation.

- *'sous_titre'* : sous-titre affiché, à droite, dans l'entête de l'animation.

Il est possible d'afficher certaines informations à l'aide des mots clés suivant :
    - nb_unite : affiche le nombre d'unités contenues dans le périmètre
    - nb_unite_total : affiche le nombre total d'unités de l'élection
    - nb_unite_arrivees : affiche le nombre d'unités envoyées à l'affichage et contenues dans le périmètre choisi
    - nb_unite_total_arrivees : affiche le nombre d'unités envoyées à l'affichage

- *'couleur du titre'*

- *'couleur du bandeau de titre'*

- *'logo'* : choix du logo à afficher dans l'animation. Le logo est affiché dans l'entête avant le titre.

- *'périmètre à afficher'* : paramétrables que dans le contexte d'une élection.

- *'type des unités à afficher'* : paramétrables que dans le contexte d'une élection.

- *'affichage'* : ce champs sert à personnaliser le contenu de l'animation.

- *'Feuille de style'* : champs servant à paramétrer la feuille de style de l'animation.

- *'script'* : champs servant à ajouter du javascript à l'animation.

- *'modele'* : champs servant à choisir un modèle pour récupérer son paramétrage.

Paramétrage du champs affichage
===============================

Le champs affichage gère la disposition et l'affichage de tous le contenu de
l'animation. Les éléments paramétrable sont les suivants :

1- La sidebar : Pour avoir une sidebar, il faut saisir le texte suivant :

[sidebar]\
paramétrage de la sidebar

.. note::
   Les éléments paramétrables de la sidebar sont :
   - Sa taille
   - Sa position
   - Si les unités qu'elle contiens sont toutes cliquables ou seulement les unités envoyées à l'affichage

2- Les blocs : Il est possible de paramétrer autant de blocs que souhaité. L'ajout de bloc se fait de la manière suivante :

[bloc1]\
paramétrage du bloc1\
[bloc2]\
paramétrage du bloc2\

.. note::
   Les blocs seront affiché dans l'ordre où ils sont écrit, la numérotation n'a pas d'impact sur l'ordre.
   Pour être afficher le paramétrage du "type" de blocs est obligatoire.

.. note::
   Les éléments paramétrables de ces blocs sont :
   - Son type
   - Sa taille
   - Le type de diagramme
   - La taille des diagrammes
   - La présence ou non de photo
   - La couleur de son entete
   - L'offset.


Détail du paramétrage des éléments :
    - Taille : concerne les blocs et la sidebar. Pour la paramétrer, il faut lui donner une valeur comprise entre 1 et 12. Par défaut, si la taille n'est pas renseignée, elle vaut 4. 

    Exemple : taille=4
    
    - Offset : introduit un décalage avant l'affichage du bloc. L'offset sert notamment à éviter qu'un bloc soit masqué par la sidebar. Il se paramétre comme la taille.

    Exemple : offset=2

    - Type : choix du blocs à afficher. Les types disponibles sont : resultat_unite, resultat_global, participation_unite, participation_globale, sieges_municipal, sieges_communautaire, sieges_metropolitain et comparaison_participation.

    Exemple : type=sieges_communautaire

    - Photo : affichage des photos des candidats. Ce paramétrage ne concerne que les blocs resultat_global et resultat_unite. Par défaut, les photos ne sont pas affichées.
    
    Exemple : photo=oui

    - Diagramme : affichage d'un diagramme de type voulu. Les types disponibles sont : camembert, donut et histogramme.
    
    Exemple : diagramme=donut

    - largeur_diagramme et hauteur_diagramme : permet de régler la taille des diagrammes (en pixel). En général, il n'y a pas besoin des les utiliser car la taille du diagramme s'adapte automatiquement à celle du bloc.
    
    Exemple :
    
    largeur_diagramme=250\
    hauteur_diagramme=300

    - Panel : couleur de l'entête des blocs. Les couleurs sont celles proposées par bootstrap. Il y a : success (vert), error(rouge), warning(orange), primary(bleu), info(bleu clair) et default(gris clair). Par défaut, c'est "primary" qui est utilisé.
    
    Exemple : panel=success

    - Cliquable : détermine si toutes les unités sont cliquables ou seulement celles envoyées à l'affichage. Ne concerne que la sidebar. Par défaut, seules les unités arrivées sont cliquables. 
    
    Exemple : cliquable=oui

    - Position : détermine si la sidebar se trouve à droite ou à gauche de l'écran. Par défaut, elle se trouve à droite.
    
    Exemple : Position=gauche

Des exemples de paramétrage sont visibles dans les animations pré-configurées.


Creation de modèle
==================

Les modèles sont des animations dont on souhaite réutiliser le paramétrage.
Pour créer un modèle, il faut commencer par créer une nouvelle animation avec le
paramétrage souhaité.
Ensuite, pour que ce modèle soit accessible dans la liste des modèles à utiliser,
il faut l'activer. Pour cela, il faut cliquer sur l'action *activer*.

Pour utiliser un modèle, il faut commencer par créer une nouvelle animation. dans
le formulaire de saisie de l'animation, il faut sélectionner le modèle voulu dans le
champs *'modèle'*. Une fois choisit, le paramétrage du modèle va être copié dans
le formulaire de l'animation.

Animations pré-configurée
=========================

Il y a 6 modèles d'animation déjà configurés dans l'application :

- *'liste municipale'*

.. figure:: liste.png

- *'Resultat'*

.. figure:: resultat.png

- *'Resultat liste gauche'*

.. figure:: res_lg.png

- *'Resultat sans cumul liste gauche'*

.. figure:: res_sans_cumul_lg.png

- *'Resultat avec graphique cumul liste gauche'*

.. figure:: res_cumul_lg.png

- *'Participation'*

.. figure:: participation.png


Ces modèles reprennent les modèles disponibles dans la v1. Ils ne sont
pas modifiables et ne peuvent pas être supprimés. En revanche, il est possible
de les rendre inactifs si ils ne sont pas utiles. Pour cela, il faut se rendre
sur l'animation voulue et cliquer sur **desactiver**.
