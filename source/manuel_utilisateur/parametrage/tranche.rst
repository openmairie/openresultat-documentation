.. _tranche:

Saisie tranche
==============

Les tranches horaires sont utilisées pour paramétrer la participation et les heures d'ouverture et de fermeture de l'élection.
Il est possible de paramétrer les tranches horaires dans la rubrique *tranche*.

.. figure:: tab_tranche.png

   Listing des tranches

Le formulaire de paramétrage des tranches est composé de deux champs :

- libelle : horaire sous la forme 00:00:00.

- ordre : ordre d'affichage, de sélection et de saisie des tranches horaires

.. figure:: form_tranche.png

   Formulaire d'une tranche
