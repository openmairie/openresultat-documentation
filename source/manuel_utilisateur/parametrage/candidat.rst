.. _candidat:

.. _parametrage_candidat:

Candidats
==========

Avant de pouvoir associer des candidats à une élection, il faut créer ces candidats. La création des candidats se fait depuis la rubrique *candidat*.

.. figure:: tab_candidat.png

   Listing des candidats

Le paramétrage des candidats est très simple puisque seulement deux champs doivent être rempli :

- libelle : nom du candidat ou de la liste de candidat

- libelle liste : (non obligatoire) non de la liste à laquelle est rattaché le candidat

.. figure:: form_candidat.png

   Formulaire d'un candidat
