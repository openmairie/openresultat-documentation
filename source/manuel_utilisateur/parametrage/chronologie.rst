.. _chronologie:

############################
Chronologie de l’application
############################

Chaque étape de la chronologie de l’application sera décrite dans la rubrique suivante.

1. Création de la Collectivité Paramétrage Collectivité
2. Création des types d'élection
3. Création des tranches horaires
4. Création des cantons
5. Création des circonscriptions
6. Création des communes
7. Création des départements
8. Création des types d'unités
9. Création/Import des unités de saisie
10. Création des périmètres
11. Création des liens entre les périmètres et les unités de saisie
12. Création des candidats
13. Création et Activation du modèle de portail web
14. Création des plans
15. Positionnement des unités sur les plans
16. Création et Activation des modèles d'animation
17. Configuration de l'élection


Etape de paramétrage


17. Paramétrage des candidats de l'élection
18. Import des inscrits
19. Paramétrage des centaines de l'élection
20. Paramétrage des animations de l'élection
21. Paramétrage des plans de l'élection


Etape de simulation


Etape de saisie


22. Saisie des centaines
23. Saisie des résultats par unités
24. Publication des résultats
25. Saisie de la participation
26. Publication de la participation
27. Accès aux animations
28. Accès à la page web


Etape de finalisation


29. Calcul du nombre de siège
30. Edition des résultats
31. Envoi des résultats à la préfecture


Archivage de l'élection