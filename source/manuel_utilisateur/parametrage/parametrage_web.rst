.. parametrage_web.rst

Tout au long du processus de saisie de l’élection des fichiers sont transférés au site web de la ville, pour que les citoyens puissent consulter les résultats en direct de chez eux.
Ce portail web est personnalisable par l'utilisateur à l'aide de modèle web.

Modèle web
===========

Le paramétrage des modèles de portail web se fait depuis la rubrique *web*.

.. figure:: tab_web.png

   Listing des modèles web

Les éléments paramétrable du modèle web sont :

- libellé : nom du modèle web qui permettra de l'identifier.

- logo : choix d'un logo a afficher sur le portail web.

- entete : titre affiché dans l'entete du portail web.

- url de la collectivite : url affichée sur le portail web qui permet de rediriger vers le site de la collectivité.

- nom du lien : texte à afficher sur le portail web et qui sera associé au lien.

- feuille de style : feuille de style css de la page web.

- script : permet d'ajouter du javascript à la page web.

- afficher les centaines: option servant à choisir si les centaines doivent être visible sur le listing des élections du portail web.

.. figure:: form_web.png

   Formulaire des modèles web

Il est possible de paramétrer plusieurs modèle de portail web. Cependant, un seul modèle sera utilisé.
Pour choisir le modèle à utiliser, il faut l'activer en cliquant sur l'action *activer* du modèle.