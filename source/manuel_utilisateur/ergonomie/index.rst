.. _ergonomie:

#########
Ergonomie
#########

.. _connexion_deconnexion:

**********************
Connexion, déconnexion
**********************

=========
Connexion
=========

Une application openMairie est une application Web, pour l'utiliser depuis un 
terminal tel qu'un ordinateur ou une tablette, il faut démarrer un navigateur
Web afin d'y saisir l'adresse et les identifiants de connexion fournis par votre
administrateur qu'il aura au préalable configuré dans le logiciel.


Navigateur Web
==============

L'application est accessible via un navigateur Web (Firefox, Chrome, Opera, ...),
pour y accéder il faut saisir l'adresse Web (URL) fournie par votre administrateur
dans la barre d'adresse.

.. figure:: ergo_url.jpg

    Saisie d'adresse dans un navigateur Web

.. note::

    Ce logiciel est développé principalement sous le navigateur Mozilla Firefox,
    il est donc conseillé d'utiliser ce navigateur pour une efficacité optimale.


Saisie des informations de connexion
====================================

Cet écran de connexion est composé de deux zones de texte et d'un bouton.

.. figure:: a_connexion_formulaire.png

    Formulaire de connexion

La figure ci-dessus présente l'écran d'identification, il faut saisir son
identifiant et son mot de passe puis cliquer sur **Se connecter**.

.. note::

    L'identifiant et le mot de passe doivent être saisis en respectant la
    casse, c'est-à-dire les minuscules et majuscules.


Connexion échouée
-----------------

Si les identifiants saisis sont incorrects, un message d'erreur apparaît et il
faut ressaisir les informations de connexion et re-cliquer sur **Se connecter**.

.. figure:: a_connexion_message_erreur.png

    Message de connexion échouée


Connexion réussie
-----------------

Si les identifiants sont corrects, vous êtes redirigé vers la page demandée sur
laquelle le message suivant doit d'afficher.

.. figure:: a_connexion_message_ok.png

    Message de connexion réussie


===========
Déconnexion
===========

Tant que toutes les fenêtres de votre navigateur ne sont pas fermées, votre 
session reste ouverte dans votre navigateur au moins pour un temps: une nouvelle 
fenêtre Firefox aura alors accès à l'application avec votre compte.
Pour une question de sécurité, il est donc important de prendre l'habitude 
de déconnecter de l'application quand vous n'en avez plus l'usage immédiat.

L'action "Déconnexion" est disponible à tout moment dans les actions
personnelles en haut à droite de l'écran.

.. figure:: a_deconnexion_action.png

   Action "Déconnexion" dans la barre d'actions personnelles

Une fois déconnecté, c'est le formulaire de connexion qui s'affiche avec un
message expliquant la réussite de la déconnexion.

.. figure:: a_deconnexion_message_ok.png

    Message de déconnexion réussie


.. _ergonomie_generale:

******************
Ergonomie générale
******************

L'application, sur la grande majorité des écrans, conserve ses composants
disposés exactement au même endroit. Cette structuration de l'application
permet donc à l’utilisateur de toujours trouver les outils au même endroit
et de se repérer rapidement. Nous allons décrire ici le fonctionnement
et l'objectif de chacun de ces composants. 

.. figure:: a_ergonomie_generale_detail.png
    
    Ergonomie générale

.. note::

    Les actions et affichages de l'application diffèrent en fonction du profil
    de l'utilisateur. Il se peut donc que dans les paragraphes qui suivent
    des actions soient décrites et n'apparaissent pas sur votre interface
    ou inversement que des actions ne soient pas décrites mais apparaissent sur
    votre interface.


===============
Le haut de page
===============

Logo
====

C'est le logo de l'application, de plus il vous permet en un seul clic de revenir
rapidement au tableau de bord.

.. figure:: a_ergonomie_logo.png
    
    Logo
    
Actions personnelles
====================

Cet élément affiche plusieurs informations importantes.

Par défaut:

* L'identifiant de l'utilisateur actuellement connecté 
* Le nom de la collectivité sur laquelle nous sommes en train de travailler. En mode multi-collectivité, une action est proposée sur ce champ pour permettre de changer de collectivité.
* Enfin l'action pour permettre de changer de mot de passe peut être proposée
* En dernier, le bouton et le mot ``Déconnexion`` pour se déconnecter 

.. figure:: a_ergonomie_actions_personnelles.png
    
    Actions personnelles
    
Raccourcis
==========

Cet élément permet d'afficher des raccourcis vers des écrans auxquels nous
avons besoin d'accéder très souvent. Par exemple, ici nous avons un lien 
vers le tableau de bord.

.. figure:: a_ergonomie_raccourcis.png
    
    Raccourcis
    
====================
La colonne de gauche
====================

Menu
====

Cet élément permet de classer les différents écrans de l'application en
rubriques. En cliquant sur l'entête de rubrique, nous accédons à la liste des
écrans auxquels nous avons accès dans cette rubrique.

Le nombre de rubriques disponibles dans le menu peut varier en fonction du
profil des utilisateurs. Un utilisateur ayant le profil *Consultation* aura
probablement accès à moins de menus qu'un utilisateur ayant le profil *Administrateur*

.. figure:: a_ergonomie_menu.png
    
    Menu


================
La zone centrale
================

Cet élément est la fenêtre active sélectionnée par le menu.

La plupart des éléments du menu conduisent à afficher dans la fenêtre soit un listing, 
soit un formulaire, dont le fonctionnement général est décrit dans le paragraphe
':ref:`ergonomie_listing_formulaire`'. 

Au démarrage, c'est le *tableau de bord* qui y est affiché. Le tableau de bord affiche
des éléments sous forme de bloc appelés *widgets*. Les widgets présentés et leur place 
dépendent du profil de droit de l'utilisateur.

Les deux vocations principales de ces widgets de tableau de bord sont :

* aider au pilotage en affichant des indicateurs et des liens vers les menus s'y rapportant
* offrir un accès transverse: cartographie, recherche inter-menu


===============
Le pied de page
===============

Actions globales
================

Cet élément permet d'afficher en permanence le nom et la version du logiciel.

Ensuite on a par défaut différents hyper-liens vers :

* la documentation
* l'espace d'échange (forum) de l'application
* la page de l'application sur le site officiel openMairie

.. figure:: a_ergonomie_actions_globales.png
    
    Actions globales



.. _ergonomie_listing_formulaire:

***********************
Listings et Formulaires
***********************

.. _ergonomie_listing:

========
Listings
========

Un listing s'affiche quand on clique sur la plupart des entrées du menu. Par 
exemple, pour le menu ``Administration > Gestion des utilisateurs > Utilisateur`` : 

.. figure:: m_ergonomie_listing_global.png

    Listing

Le **titre** indique le menu qui a été demandé; ici : ``Administration  => Droit``

Le listing est affichée dans un **onglet** dont le nom rappelle le sous-menu; ici : 
``Droit``

**C'est à partir du listing qu'on peut demander l'ajout d'un enregistrement** dans
le cas ou l'ajout est autorisé. Cela se fait grâce au bouton |bouton_ajout| situé 
dans l'en-tête de la première colonne. 


La zone de pagination
=====================

Cette zone affiche le nombre d'enregistrements du listing, en tenant compte des 
filtres de recherche éventuels en cours. Par défaut, le listing est paginée par 15,
et on peut soit faire défiler les pages, soit sélectionner la page à afficher.

.. figure:: m_ergonomie_listing_pagination.png

    Pagination de listing


La zone de recherche
====================

Quel que soit le mode de recherche:
   * on utilise le caractère * pour indiquer un ou plusieurs caractères inconnus
   * il faut vérifier dans la note sous la zone de recherche si des * sont ajoutés implicitement ou pas en début ou fin; par défaut ils le sont
   * une zone laissée vide ou avec seulement des blancs est ignorée
   * pour chercher plusieurs valeurs à la fois, il faut les séparer par des virgules dans la zone de saisie. Par exemple, pour chercher 120 ou 121 ou 124 ou 127: ``120,121,124,127``
   * la casse (minuscule/majuscule) est ignorée
   * les accents et caractères de ponctuation sont ignorés

Par défaut, une zone de recherche standard est affichée en haut à droite, s'il n'a pas 
été prévu une recherche avancée pour ce listing.

On peut rechercher une ou des valeurs en la saisissant, en sélectionnant des champs 
parmi lesquels on cherche (*Tous* par défaut), et cliquer sur ``Recherche`` :

.. figure:: a_ergonomie_exemple_listing_recherche_standard.png
   
   Recherche standard
   
Si une recherche avancée a été prévue, en cliquant sur le libellé ``Afficher la recherche ...``, 
on peut basculer entre les modes recherche avancée et simple.

En recherche avancée : 

.. figure:: a_ergonomie_exemple_listing_recherche_avancee.png

   Recherche avancée
   
* chaque zone de saisie cherche dans le champ associé
* sur la droite en haut du listing, il peut y avoir des icones qui permettent:

 - d'imprimer le résultat: |imprimante|
 - d'exporter le résultat: |csv|
 
  
En recherche simple: 
   
.. figure:: a_ergonomie_exemple_listing_recherche_simple.png

   Recherche simple

* une seule zone de saisie sert à chercher
* on peut chercher soit dans un champ, soit dans tous les champs à la fois, comme avec la recherche standard


Une fois la recherche effectuée, le listing affiche les éléments correspondants. 

.. _ergonomie_listing_entete:

Entête des colonnes
===================

**Ajout**

L'icone |bouton_ajout|, s'il est présent dans l'en-te de la 1ère colonne, 
permet d'ajouter un nouvel enregistrement. 
D'autres actions transverses à tous les enregistrements peuvent être proposées 
ici, chacune est alors représentée par une icone.

**Tri**

Un listing ne peut être trié que sur une seule colonne à la fois. Une icône "triangle" 
devant le titre de la colonne permet de changer l'ordre de trier :

* pointe à l'horizontale |trianglehorizontal| : sans tri demandé par l'utilisateur (tri par défaut); en général il s'agit d'un tri par libellé
* pointe vers le  bas |trianglebas| : tri "dans le sens naturel de lecture", du plus petit au plus grand
* pointe vers le haut |trianglehaut| : tri "dans le sens inverse du sens naturel de lecture", du plus grand au plus petit

.. note::

  pour certaines applications, les icônes triangle sont parfois inversées par 
  rapport au standard openMairie :
  
   * pointe en haut |trianglehaut| : tri croissant
   * pointe en bas |trianglebas| : tri décroissant

**Indicateur d'éligibilité à la recherche**

Une icône |loupe| après le titre de la colonne indique qu'une recherche peut 
être faite sur les valeurs de cette colonne.


Consultation
============

Par défaut, pour consulter un enregistrement du listing, on peut cliquer :

* Soit dans la première colonne sur l'icone |loupedossier|
* Soit sur n'importe quelle valeur de la ligne
 
Un écran formulaire de consultation s'affiche alors, permettant, de consulter 
la totalité des informations relatives à la ligne sélectionnée, et d'effectuer
des actions sur cet enregistrement.

Dans cette première colonne, d'autres actions que la consultation peuvent être 
proposées: chacune est alors représentée par une icone. 


.. _ergonomie_formulaire:

===========
Formulaires
===========


Les formulaires sont de deux types : le formulaire de consultation et le 
formulaire d'ajout ou mise à jour.

Ils ont en commun un certain nombre d'éléments : le retour, les zones 
repliées, les onglets.


Les éléments communs aux formulaires de consultation et mise à jour
===================================================================

**Les boutons retour**

Sur la partie gauche, en haut et en bas de l'écran, se trouve un lien 
|retour| permettant de revenir à l'écran précedent.

Si on est sur un écran de consultation, il ramène au listing.

Si on est sur un écran de modification, il ramène à l'écran de 
consultation.

.. note::

  Lorsqu'on clique sur ''Retour'', il n'y a pas d'enregistrement des 
  modifications effectuées, les saisies sont perdues.

**Les zones repliées**

Afin de présenter immédiatement les informations les plus importantes, 
certaines informations sont présentées repliées. Pour consulter le
contenu du bloc, il suffit de le déplier en cliquant dessus.

.. figure:: a_ergonomie_fieldset_closed.png

    Fieldset replié

**Les onglets** :

La fiche d'un élément est le premier onglet. Les autres onglets affichent les 
sous-listings des éléments d'une autre nature qui lui sont directement rattachés. 

Par exemple, pour un *Profil* de droit:  

* les compositions de *tableaux de bord* rattachés à ce profil
* les *droits* rattachés à ce profil
* les *utilisateurs* rattachés à ce profil

Ces listings peuvent être filtrés : à chaque caractère saisi, le filtre est 
appliqué immédiatement sur toutes les colonnes éligibles du listing.

.. figure:: a_ergonomie_exemple_onglet_listing_recherche_live.png

   Formulaire sous-listing dans un onglet

Le formulaire de consultation
=============================

**Les boutons d'action** :

Dans le formulaire de consultation d'un enregistrement, sur la partie droite de 
l'écran se trouve un bloc avec les boutons permettant d'effectuer une action sur
cet enregistrement, telle qu'une modification, une suppression ou afficher une 
édition PDF.

.. figure:: a_ergonomie_portlet_actions_contextuelles.png

   Bloc des actions d'un formulaire de consultation

Les actions présentées sont celles qui sont autorisées en termes de droit ainsi qu'en
fonction du contexte. On peut par exemple interdire à tous de supprimer un élément dont 
dépendent d'autres éléments...

Un clic sur *Modifier* ou *Supprimer* charge le formulaire de mise à jour correspondant
sur lequel se trouve le bouton de validation permettant d'enregistrer la modification 
ou la suppression.

Pour rappel, la création (ajout) se fait par un bouton présent sur le listing : |bouton_ajout|

Le formulaire de mise à jour
============================

**Les zones obligatoires** : Leur libellé est suivi d'une * .

**L'enregistrement** :

L'enregistrement des modifications (ajout, modification ou suppression) ne se fait
qu'après avoir cliqué sur le bouton de validation situé à gauche de l'écran, en haut
et en bas, à côté du bouton *Retour*.

Le libellé du bouton varie suivant le type de mise à jour demandé : *Ajouter*, 
*Modifier*, *Supprimer*.

.. figure:: a_ergonomie_exemple_formulaire_modification.png

   Validation de formulaire

Lorsque l'enregistrement est effectué, le formulaire de consultation s'affiche de 
nouveau avec confirmation de l'enregistrement.

.. figure:: a_ergonomie_exemple_formulaire_modification_message_validation.png

   Message de confirmation

****************
Tableau de bord
****************

.. _widgets:

=======
Widgets
=======

 * Widget "Élections"
 * Widget "Animations"
 * Widget "Portail Web"
 * Widget "Carte des unités"

======================================
Les droits et profils des utilisateurs
======================================
 
Les droits et profils des utilisateurs permettent de limiter l'accès aux
informations et aux actions uniquement aux personnes autorisées. Chaque
utilisateur est associé à un profil. Le profil correspond à un ensemble
de permissions de l'utilisateur, par défaut il existe cinq profils :
 
 #. Consultation,
 
 #. Utilisateur limité,
 
 #. Utilisateur,
 
 #. Super utilisateur,
 
 #. Administrateur.
 
Chaque page de l'application est associée à un profil. Pour chaque accès à une
page, l'application vérifie si l'utilisateur a un profil supérieur ou égal au
profil de la page consultée, si c'est le cas l'utilisateur à donc le droit
d'accéder aux informations.

==================
Widget "Élections"
==================

Ce widget donne la liste de toutes les élections en cours.
Il permet également de voir l'étape à laquelle se trouve l'élection (paramétrage, saisie, etc.).

Il est possible d'accéder à une élection directement depuis cette liste en cliquant dessus.

.. figure:: widget_1.png

    Widget "Élections"

===================
Widget "Animations"
===================

Le widget "Animations" permet de voir en un coup d'oeil la liste de toutes les animations des élections en cours.

Elle permet également d'accéder rapidement à une animation en cliquant dessus.

.. figure:: widget_2.png

    Widget "Animations"

====================
Widget "Portail Web"
====================

Ce widget permet d'accéder rapidement au listing des élections sur le portail web.
Pour cela, il suffit simplement de cliquer dessus.

.. figure:: widget_3.png

    Widget "Portail Web"

=========================
Widget "Carte des unités"
=========================

Ce widget permet de visualiser l'emplacement des unités sur la carte.

Il permet également de visualiser les résultats des candidats enregistrés pour ces unités.
Pour cela, cliquer sur le marqueur de l'unité. Accéder ensuite à l'onglet *Infos* dans le menu a droite de la carte.
Dans la rubrique *MARQUEUR(S)*, cliquer sur le nom de l'unité ppour afficher la liste des résultats (par élection) enregistrés.

.. figure:: widget_4.png

    Widget "Carte des unités"

.. Raccourcis pour avoir des images affichées en ligne

.. |bouton_ajout| image:: a_ergonomie_icone_ajouter.png
.. |trianglehorizontal| image:: m_ergonomie_listing_triangle_horizontal.png 
.. |trianglehaut| image:: m_ergonomie_listing_triangle_haut.png
.. |trianglebas| image:: m_ergonomie_listing_triangle_bas.png
.. |loupe| image:: a_ergonomie_icone_recherche_possible_sur_cette_colonne.png
.. |loupedossier| image:: a_ergonomie_icone_visualiser.png
.. |imprimante| image:: m_ergonomie_icone_pdf_listing.png
.. |csv| image:: a_ergonomie_icone_exporter_csv.png          
.. |retour| image:: a_ergonomie_lien_retour.png
