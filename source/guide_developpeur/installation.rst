.. _installation:

############
Installation
############

.. note::

    openRésultat est une application sensible, nécessitant un paramétrage précis.
    Un mauvais paramétrage peut entrainer des erreurs dans le calcul des résultats
    électoraux et des transferts erronés en Préfecture. Ni l'équipe du projet
    openRésultat ni le chef de projet ne peuvent être tenus pour responsables
    d'un éventuel dysfonctionnement comme ceci est précisé dans la licence jointe.
    Vous pouvez, si vous le souhaitez, faire appel a un prestataire spécialisé
    qui peut fournir support, hot-line, maintenance, et garantir le fonctionnement
    en environnement de production.


**********
Pré-requis
**********

Vous devez avoir installé :

- un serveur web (apache, ...)
- PHP
- le moteur de base de donnees PostGreSQL avec l'extension PostGIS


Sous windows, il est facile de trouver de la documentation pour l'installation
de ces éléments en utilisant wamp (http://www.wampserver.com/) par exemple.


Sous Linux, il est facile de trouver de la documentation pour l'installation de
ces éléments sur votre distribution.


***********
Déploiement
***********

Installation des fichiers de l'applicatif
=========================================

Télécharger l'archive zip
-------------------------

http://adullact.net/frs/?group_id=322


Décompresser l'archive zip dans le répertoire de votre serveur web
------------------------------------------------------------------

- Exemple sous windows dans wamp : wamp/www/openresultat
- Exemple sous linux avec debian : /var/www/openresultat


Création et initialisation de la base de données
================================================

Créer la base de données
------------------------

Il faut créer la base de données dans l'encodage UTF8. Par défaut la base de données s'appelle openresultat.


Dans un environnement debian :

.. code-block:: bash

  createdb openresultat


Initialiser la base de données
------------------------------

Il faut initialiser les tables, les séquences et données de paramétrage grâce au script data/pgsql/install.sql


Dans un environnement debian depuis le répertoire data/pgsql/ :

.. code-block:: bash

  psql openresultat -f install.sql


Configuration de l'applicatif
=============================

Positionner les permissions nécessaires au serveur web
------------------------------------------------------

Dans un environnement debian : 

.. code-block:: bash

  chown -R www-data:www-data /var/www/openresultat


Configuration de la connexion à la base de données
--------------------------------------------------

.. note::

   Si le répertoire `dyn/` n'existe pas il faut le créer.

La configuration se fait dans le fichier `dyn/database.inc.php` (si le
script n'existe pas il faut le créer) :

.. code-block:: php

    <?php
    ...
    // PostGreSQL
    $conn[1] = array(
        "openRésultat", // Titre 
        "pgsql", // Type de base
        "pgsql", // Type de base
        "postgres", // Login
        "postgres", // Mot de passe
        "tcp", // Protocole de connexion 
        "localhost", // Nom d'hote
        "5432", // Port du serveur
        "", // Socket
        "openresultat", // nom de la base
        "AAAA-MM-JJ", // Format de la date
        "openresultat", // Nom du schéma
        "", // Préfixe
        null, // Paramétrage pour l'annuaire LDAP
        null, // Paramétrage pour le serveur de mail
        null, // Paramétrage pour le stockage des fichiers
    );
    ...
    ?>

*************************
Connexion à l'application
*************************

Ouverture dans le navigateur
============================

http://localhost/openresultat/

'localhost' peut être remplacé par l'ip ou le nom de domaine du serveur.


Login
=====

* Utilisateur "administrateur" : 
   - identifiant : admin
   - mot de passe : admin

Le message de bienvenue doit être affiché "Votre session est maintenant ouverte."


***************
En cas d'erreur
***************

Activer le mode debug
=====================

.. note::

   Si le répertoire `dyn/` n'existe pas il faut le créer.

Il est possible d'activer le mode debug pour visualiser les messages d'erreur
détaillés. Dans le fichier `dyn/debug.inc.php` (si le script n'existe pas il
faut le créer), il faut commenter le mode production et décommenter le mode
debug.

Mode production :

.. code-block:: php

    <?php
    (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
    require_once PATH_OPENMAIRIE."om_debug.inc.php";
    //define("DEBUG", EXTRA_VERBOSE_MODE);
    //define("DEBUG", VERBOSE_MODE);
    //define("DEBUG", DEBUG_MODE);
    define("DEBUG", PRODUCTION_MODE);
    ?>

Mode debug :

.. code-block:: php

    <?php
    (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
    require_once PATH_OPENMAIRIE."om_debug.inc.php";
    //define("DEBUG", EXTRA_VERBOSE_MODE);
    //define("DEBUG", VERBOSE_MODE);
    define("DEBUG", DEBUG_MODE);
    //define("DEBUG", PRODUCTION_MODE);
    ?>
