.. _mises_a_niveau:

##############
Mises à niveau
##############

Mettre à niveau openRésultat de 2.0.0 vers 2.1.0
================================================

* :ref:`Changements de la version 2.1.0<history_version_2_1_0>`.

* Sauvegarder les fichiers de l'applicatif ainsi que la base de données.

* Télécharger l'archive de la nouvelle version et la dézipper.

* Déplacer/Copier les répertoires `dyn/`, `var/`, `aff/res/`, `web/res/`, `web/img/`, `web/dyn/`, depuis le répertoire de l'ancienne version vers le répertoire de la nouvelle version.

* La structure de la base de données n'a pas changée depuis la version 2.0.0. Le script SQL :file:`data/pgsql/v2.1.0.sql` est donc vide.


Mettre à niveau openRésultat de 2.0.0-rc3 vers 2.0.0
====================================================

* :ref:`Changements de la version 2.0.0<history_version_2_0_0>`.

* Sauvegarder les fichiers de l'applicatif ainsi que la base de données.

* Télécharger l'archive de la nouvelle version et la dézipper.

* Déplacer/Copier les répertoires `dyn/`, `var/`, `aff/res/`, `web/res/`, `web/img/`, `web/dyn/`, depuis le répertoire de l'ancienne version vers le répertoire de la nouvelle version.

* La structure de la base de données n'a pas changée depuis la version 2.0.0-rc3. Le script SQL :file:`data/pgsql/v2.0.0.sql` est donc vide.


Mettre à niveau openRésultat de 2.0.0-rc2 vers 2.0.0-rc3
========================================================

* :ref:`Changements de la version 2.0.0-rc3<history_version_2_0_0_rc3>`.

* Sauvegarder les fichiers de l'applicatif ainsi que la base de données.

* Télécharger l'archive de la nouvelle version et la dézipper.

* Déplacer/Copier les répertoires `dyn/`, `var/`, `aff/res/`, `web/res/`, `web/img/`, `web/dyn/`, depuis le répertoire de l'ancienne version vers le répertoire de la nouvelle version.

* Appliquer le script SQL de mise à jour de la base de données :file:`data/pgsql/v2.0.0-rc3.sql`.


Mettre à niveau openRésultat de 2.0.0-rc1 vers 2.0.0-rc2
========================================================

* :ref:`Changements de la version 2.0.0-rc2<history_version_2_0_0_rc2>`.

* Sauvegarder les fichiers de l'applicatif ainsi que la base de données.

* Télécharger l'archive de la nouvelle version et la dézipper.

* Déplacer/Copier les répertoires `dyn/`, `var/`, `aff/res/`, `web/res/`, `web/img/`, `web/dyn/`, depuis le répertoire de l'ancienne version vers le répertoire de la nouvelle version.

* Appliquer le script SQL de mise à jour de la base de données :file:`data/pgsql/v2.0.0-rc2.sql`.


Mettre à niveau openRésultat de la version 1.16 vers 2.0.0-rc1
==============================================================

Il n'y a pas de mise à niveau. Une nouvelle installation est recommandée.

* :ref:`Changements de la version 2.0.0-rc1<history_version_2_0_0_rc1>`.


Mettre à niveau openRésultat de 1.13 vers 1.14
==============================================

* Sauvegarder les fichiers de l'applicatif ainsi que la base de données.

* Appliquer le script de mise à jour de la base de données 
  `data/mysql/ver_1.14.sql`.

* Déplacer tous les fichiers .pdf du répertoire `pdf/` vers le répertoire 
  `tmp/`.::

   mv pdf/*.pdf tmp/

* Déplacer tous les répertoires de résultats générés à la racine du répertoire
  `aff/` vers le répertoire `aff/res/`.::

   mv aff/REF* aff/res/
   mv aff/PRE* aff/res/
   ...

* Configurer le nouvel affichage web.
  
  Renseigner la liste des plans.

  Il faut faire une entrée pour chaque plan que possède la collectivité,
  la clés de cette entrée doit être le nom de l'image. Chaque entrée comporte
  trois informations :

  - img : lien vers l'image
  - libelle : nom du plan
  - id : identifiant du plan

  Exemple d'utilisation de la variable $plans :

  $plans = array(
    "centre.gif" => array (
    
        'img' => 'img/plan/centre.gif',
        
        'libelle' => 'Secteur Nord',
        
        'id' => 'secteur-nord',
        
    ),
    
    "quartier.gif" => array (
    
        'img' => 'img/plan/quartier.gif',
        
        'libelle' => 'Secteur Sud',
        
        'id' => 'secteur-sud',
        
    ),
    
  );
  
  Renseigner les informations sur la collectivité.

  Liste des entrées de la variable $infos_collectivite :

  - title : nom de la collectivité
  - logo : lien vers le logo (img/logo.png)
  - url : lien vers le site web de la collectivité (doit commencer par http)
