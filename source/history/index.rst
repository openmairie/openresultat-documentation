.. _history:

######################
Historique de versions
######################

.. _history_version_2_1_0:

2.1.0 (31/03/2022)
==================

* Correction : Diverses corrections dans les éditions pdf de l'élection. Ticket #9835.

* Correction : Modification des imports pour pouvoir importer des unités et des inscrits sans utiliser l'id issus du REU et en utilisant uniquement le code de l'unité. Ticket #9815.

* Évolution : Amélioration de l'ergonomie des formulaires de saisie des résultats et des centaines. Ticket #9817.

* Correction : Réduction de la charge du traitement du reset de l'élection. Ticket #9816.

* Correction : Correction du format de l'export préfecture pour avoir des codes candidats sur 4 caractères pour les présidentielles. Ticket #9820.


.. _history_version_2_0_0:

2.0.0 (28/06/2021)
==================

* Packaging : Publication de la version stable.


.. _history_version_2_0_0_rc3:

2.0.0-rc3 (23/06/2021)
======================

* Évolution : Ajout de l'état résultat préfecture par périmètre. Ticket #9536.

* Correction : La permission pour l'accès à la saisie des centaines depuis une action de la fiche *election_unite* est incorrecte. Ticket #9527.

* Correction : Les fichiers issus des répertoires web de la v1 sont compatibles avec la v2. Ticket #9522.

* Correction : Correction du total dans les champs de fusion servant à afficher les résultats. Ticket #9535.

* Correction : Correction de la dépublication des résultats sur le portail web et les animation. Ticket #9537.

* Correction : Correction de l'ordre d'affichage des unités sur l'édition des résultats par périmètre. Ticket #9532.

* Correction : Les champs de fusion dans les éditions pdf provoquaient des lenteurs.

* Correction : La largeur du champ de fusion *etat_resultat_globaux_opt2* est désormais figée.

* Correction : Correction du problème de compatibilité (libxml2 2.9.4 et libxml2 2.9.9) sur le portail web.

* Correction : Ajout d'une taille min pour la case des résultats des candidats sur le portail web pour éviter d'avoir des résultats sur plusieurs lignes. Ticket #9540.

* Correction : Correction erreur de base de données dans le formulaire de liaison des unités, dans le cas où le type d'unité contenu de l'unité parente n'est pas renseigné. Ticket #9542.

* Correction : Correction du nombre de votant dans le listing de la participation en délégation. Ticket #9538.


.. _history_version_2_0_0_rc2:

2.0.0-rc2 (14/06/2021)
======================

* Évolution : Amélioration des requêtes d'éditions par l'ajout de la requête *election_unite* et l'optimisation de la requête *election*. Ticket #9525.

* Évolution : Toutes les éditions de l'élection sont désormais disponibles pour les centaines, à l'exception des éditions "participation" et "proclamation des résultats répartition des sièges". Ticket #9519.

* Évolution : Il est désormais possible de choisir les couleurs pour le diagramme des blocs de participation ainsi que le titre en entête de la sidebar  depuis le champ "affichage" de l'animation. Ticket #9508.

* Évolution : L'import des unités de saisie permet maintenant d'importer des unités autre que des bureaux de vote. Ticket #9509.

* Amélioration : Tests & Documentation.

* Correction : Mise à niveau framework openMairie > 4.9.10. Ticket #9524.

* Correction : Reprendre la gestion des permissions. Ticket #9513.

* Correction : Notices PHP (PHP Notice: Undefined index: age_moyen_mep | siege_mep in gen/obj/election_candidat.class.php on line 177). Ticket #9506.

* Correction : Incompatibilité <= PHP7.2 (PHP Parse error: syntax error, unexpected ')' in obj/election.class.php on line 2071). Ticket #9507.

* Correction : Warning PHP (PHP Warning: strtr() expects parameter 1 to be string, array given in core/om_formulaire.class.php on line 762). Ticket #9504.

* Correction : La liste des unités est maintenant transmise à l'affichage lors de la création des animations. Elle est également mise à jour en cas de modification du périmètre. Ticket #9515.

* Correction : Lors de la création de l'élection le champ envoi initial est vrai par défaut. Ticket #9518.

* Correction : Ajout de la colonne émargement dans l'export csv pour la préfecture. Mise à zéro des résultats des candidats dans l'export préfecture si aucun résultat n'est renseigné. Ticket #9517.

* Correction : Problème d'encodage dans la méthode d'envoi de mail application::sendMail(). Ticket #9505.

* Correction : Mise à jour de l'état de la saisie, lors de la publication des résultats via les cases à cocher, afin que les unités soient bien considérées comme "envoyées". Ticket #9523.

* Correction : Les metadatas des images liées au plan ne sont plus transmises dans le json de paramétrage des plans, dans le répertoire web. Évite que l'écriture du json du paramétrage des plans échoue si il y a des accents dans les noms de ces images. Ticket #9520. 

* Correction : PHP notice : Undefined variable: voix in obj/election.class.php on line 3467. Ticket #9511.

* Correction : Modification de la méthode d'écriture du csv préfecture pour formater correctement les codes préfecture (candidat, bureau, etc.). Ticket #9514.

* Correction : Lors de l'import des unités on initialise désormais tous les codes préfecture à 0 plutôt que d'utiliser le code de l'élément importé qui peut porter à confusion. Ticket #9526.

* Correction : L'ordre d'affichage des candidats sur l'animation ne correpondait pas aux numéros de panneau. Ticket #9510.

* Correction : Suppression d'éléments inutilisés en base de données. Ticket #9521.


.. _history_version_2_0_0_rc1:

2.0.0-rc1 (20/05/2021)
======================

* Correction - Mise à niveau framework openMairie > 4.9.9.

* Evolution - Passage des bureaux de vote aux unites de saisie.
  Nouveau modèle de données permettant de réaliser la saisie des résultats
  avec des unités dont le type est paramétrable (commune, bureau ou autre). Les 
  unites peuvent également être des perimetres et contenir d'autres unites.
  Ticket #9400. [Carole Garcin][atReal]

* Evolution - Ajouter les attributs emargement et procuration
  Ajout des attributs emargements et procurations dans le modèle. Ces attributs
  servent lors de la saisie des résultats.
  Ticket #9401. [Carole Garcin][atReal]

* Evolution - Hiérarchiser les types d’unités
  Hiérarchisation entre les unités de saisie permettant de structurer plus facilement
  les périmètres, en gardant une cohérence entre les unités. La hiérarchisation
  consiste à donner une valeur aux différents types d'unité. Une unité de valeur
  de hiérarchie élevée ne pourra ainsi pas être contenue par une unité de hiérarchie
  inférieure. Par exemple, un bureau de hiérarchie 1 ne pourra pas contenir une mairie
  de hiérarchie 2. Mais la mairie pourra contenir des bureaux car leur valeur est inférieure. 
  Ticket #9411. [Carole Garcin][atReal]

* Evolution - Ajouter les tables canton, circonscription, commune et département
  Ajout de quatre tables dans la base de données, permettant de récupérer les informations
  nécessaires, à un bureau de vote, lors de la transmission des résultats à la préfecture.
  Ticket #9413. [Carole Garcin][atReal]

* Evolution - Faire en sorte qu'une unité ne puisse contenir que des unités du type voulu
  Afin de cadrer la structure des périmètres, un périmètre ne peut contenir qu'un seul type
  d'unité. Ce type est défini par le type voulu de l'unité servant de périmètre. Ainsi
  lorsque l'on crée les liens entre les unités, seules les unités "périmètre" pourront être des
  unités parentes. De plus, seules les unités ayant le type recherché pourront être rattaché à
  l'unité parente.
  Ticket #9414. [Carole Garcin][atReal]

* Evolution - Différenciation des unités de type bureau de vote
  Dans une élection les bureaux de vote ont un caractère particulier par rapport aux autres unités.
  Ils sont à la base de la récupération des résultats et on besoin d'information supplémentaires,
  par rapport aux autres unités, pour être caractérisé et pour que leurs résultats puissent être
  transmis.
  Le but de cette évolution est donc de mettre en place le comportement particulier des bureaux de vote.
  Cela consiste à utiliser uniquement les résultats des unités ayant ce comportement pour faire 
  les animations, les éditions, les exports, le calcul des sièges et les résultats de la page web.
  De plus, seules les unités ayant ce comportement ont besoin des codes cantons, circonscriptions,
  dept et communes. 
  Ticket #9415. [Carole Garcin][atReal]

* Evolution - Gestion des périmètres
  Les périmètres sont des groupes d'unités par conséquent, il ne se comportent pas exactement comme les
  unités qu'ils contiennent. Cette évolution consiste à gérer le comportement particulier des périmètres.
  Ainsi, les résultats et la participation des périmètres sont automatiquement calculés à partir des
  unités qu'ils contiennent. L'affichage des animations a également évolué pour permettre à l'utilisateur
  de choisir les informations qu'il souhaite afficher (périmètre et type d'unité). Pour finir,
  les unités et les périmètres sont différenciées dans les listes d'unités et seule les unités non
  périmètres sont modifiables par l'utilisateur dans le cadre de la saisie.
  Ticket #9418. [Carole Garcin][atReal]

* Evolution - Suppression de fonctionnalités
  Suppression de trois fonctionnalités : le transfert des candidats depuis une autre élection, le transfert
  des inscrits depuis une autre élection et la suppression d'une élection cloturée. Ces trois fonctionnalités
  ne sont pas souhaitée dans la version finale.
  Ticket #9429. [Carole Garcin][atReal]

* Evolution - Gestion des centaines
  Lors d'une élection les centaines permettent de faire des projections des résultats obtenus avant
  la fin des votes. La saisie par centaine est donc équivalente à une élection la différence étant
  qu'elle a un nombre de votant fixé pour chaque unités.
  La mise en place des centaines consiste donc à utiliser une élection comme référence et copier le
  paramétrage de cette élection (unités, candidats, tranches horaires et nombre d'inscrits pour
  chaque unité). Le nombre de votant est paramétrable par l'utilisateur. La centaine se comportant
  comme une élection l'utilisateur peut ensuite l'utiliser comme une élection et faire des éditions,
  des exports ou des animations des résultats
  Ticket #9430. [Carole Garcin][atReal]

* Evolution - Personnalisation de l'animation
  Permettre à l'utilisateur de personnaliser l'animation en lui permettant notamment de :
  - personnaliser l'entête en chosissant le titre, le sous-titre et la couleur
  - choisir les éléments à afficher (sidebar, blocs) et paramétrer ces éléments (diagramme,
  photo, couleur de l'entête du panel, etc.)
  - choisir les diagrammes à afficher
  - d'associer des photos et des couleurs aux candidats de l'élection
  - d'ajouter un logo à l'animation
  - définir des modèles d'animation qu'il pourra réutiliser par la suite
  Ticket #9433. [Carole Garcin][atReal]

* Evolution - Import des unités de saisie
  Permettre à l'utilisateur d'importer les unités de saisie de l'élection à l'aide
  d'un fichier csv issus D'ELIRE
  Ticket #9463. [Carole Garcin][atReal]

* Evolution - Gestion et personnalisation des plans
  Permettre à l'utilisateur d'afficher des plans sur le portail web et de positionner
  les unités sur ces plans. Il peut également choisir les icônes représentant ces unités,
  ainsi que le texte à afficher sur ces icônes et la couleur du texte.
  Ticket #9402. [Carole Garcin][atReal]

* Evolution - Personnalisation du portail web
  Permettre  à l'utilisateur de personnaliser le portail web de l'élection. D'une part en
  lui donnant accès à la feuille de style css de la page web. D'autre part en lui donnant
  la possibilité de choisir l'entête de la page, de faire un lien vers le site web de la
  collectivité ou d'ajouter un logo.
  Ticket #9464. [Carole Garcin][atReal]

* Evolution - Workflow et ergonomie
  Mise en place du workflow de l'élection. Ajout des options de publication automatique des
  résultats, de publication des résultats présentant une erreur de saisie, de calcul auto
  des votes exprimés et de conservation des résultats après la simulation.
  Mise en place du sous-formulaire "centaine(s)" de l'élection et du formulaire de saisie des
  centaines.
  Mise en place du tableau de bord de l'élection permettant d'avoir des informations différentes
  à chaque étape du workflow.
  Ticket #9466. [Carole Garcin][atReal]

* Evolution - Délégation de saisie
  La délégation de saisie consiste à choisir les acteurs pouvant saisir les résultats et/ou
  la participation par unités. La délégation de saisie est une option de l'élection.
  Cette délégation est accompagné de vérification pour éviter les erreurs de saisie : la méthode
  de vérification de conflits et le workflow de la validation.
  Ajout du formulaire de saisie de la participation par unité dans le cadre de la délégation de la
  participation.
  Ticket #9495. [Carole Garcin][atReal]

* Evolution - Reprise des éditions de la v1
  Reprise de toutes les éditions de la v1 à l'aide d'état, sous-état, champs de fusion et
  variable de remplacement.
  Ticket #9493. [Carole Garcin][atReal]

* Evolution - Gestion des élections métropolitaine 
  Ajout de la gestion des élections métropolitaine.
  Ticket #9503. [Carole Garcin][atReal]


Elle est développée avec les principes suivants :

- elle fonctionne sur le dernier framework openMairie 4.9.7  qui permet la compatibilité php 7.4
- elle est migrée sur postgres avec postgis (abandon de mysql)
- elle supprime les variables session qui posent problème lors de saisie d'élections multiples
- elle garde le modéle relationnel d'openresultat (election archivée)
- les tables résultat et participation sont supprimées (plus de contrainte sur le nombre de candidats et le nombre de  bureaux)
- les cumuls sont faits dans des vues 
- les champs calculés (taux pourcentage) sont suprimés
- les affichages "aff" et "web" de la version 1.16 sont conservés
- les notions de canton et circonscription sont élargies à la notion de périmètre 
- la commune peut être incluse ou le périmètre peut inclure la commune
- Les périmètres peuvent être typés "canton" ou "circonscription" et ont une propriété code prefecture. 
- il n y a qu'un périmètre associé par élection.
- les états résultat et proclamation ont été repris.
- la classification politique : groupe et parti des candidats est abandonnée ainsi que le module analyse
- le module pour le calcul des élus de la liste municipale est mis sous forme d'action dans election
- les bureaux sont géolocalisés avec l'API de la BAN (base d'adresse nationale)
- il est rajouté le module interne SIG pour la carte des bureaux et des périmètres 
- le transfert des inscrits (API openElec) sera repris ultérieurement.
- il est possible de transférer les inscrits et les candidats d'une élection princpale sur les centaines
- une action de vérification permet de contrôler le paramétrage de l'élection


v1.16.1 (13/04/2017)
====================

* Ajout d'une case à cocher sur le formulaire de génération du fichier CSV à destination de la préfecture, permettant d'ajouter dans le fichier le nombre de votant d’après les feuilles d’émargements.


v1.16 (23/03/2017)
==================

* Ajout de l'option de publication des résultats définitifs.
* Modification des résultats globaux dans l'animation en ajoutant le nombre d'inscrit des bureaux arrivés seulement. De plus un pourcentage de participation est calculé sur cette nouvelle donnée.
* Ajout de la possibilité de modifier le thème de l'animation directement depuis l'URL.
* Ajout de la possibilité de renseigner le nombre d'inscrit depuis la saisie des résultats à condition que l'option "votant2_procuration_saisie_form" soit activée.
* Ajout du nombre de votants sur liste d'émargement dans le fichiers CSV des résultats transmis en préfecture.
* Ajout du nouvel affichage "Résultats (liste des bureaux à gauche)" identique à l'affichage "Résultats" mais avec la liste des bureaux à gauche.
* Ajout du nouvel affichage "Résultats sans cumul (liste des bureaux à gauche)" permettant d'afficher la liste des bureaux à gauche et les résultats du bureau sélectionné à droite.
* Ajout du nouvel affichage "Résultats avec graphique du cumul (liste des bureaux à gauche)" permettant d'afficher un graphique pour les résultats cumulés et la liste des bureaux à gauche de l'écran.
* Ajout du titre "RÉSULTATS GLOBAUX" pour la colonne des résultats cumulés dans l'animation.
* Lors de la création d'une élection, la fréquence de participation est maintenant une donnée obligatoire, ce qui évite une erreur de base de données quand elle n'est pas renseignée.


v1.15 (19/03/2015)
==================

* Modification du formulaire des types d'élection pour ajouter plusieurs
  élections du même type sur la même période.
* Correction de l'affichage de certains états.
* Amélioration du contrôle de saisit dans la plupart des formulaires.
* Modification du nom des champs pour les candidats, ce n'est pas forcément nom
  et prénom à chaque fois. Pour chaque écran de l'application, le libellé 1
  (anciennement le nom) sera affiché avant le libellé 2 (anciennement le prénom)
  lorsqu'il s'agit d'un candidat.


v1.14 (21/05/2014)
==================

* Règlementaire : gestion séparée des votes blancs et des votes nuls.
* Remplacement des affichages WEB, MOBILE et BORNE par un affichage unique
  SITE WEB ADAPTATIF (web responsive design).
* Suppression de l'affichage WEB GOOGLE.
* Ajout d'un état PDF 'résultats globaux' supplémentaire.
* Ajout d'un état PDF 'proclamation' supplémentaire.


v1.13 (12/03/2014)
==================

* Ajout de la gestion de la répartition des sièges pour l'élection des 
  conseillers communautaires lors d'une élection municipale.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.
* Ajout de trois états PDF 'proclamation'.
* Ajout d'un état PDF 'résultats globaux'.


v1.12 (29/03/2012)
==================

* Nouvelle interface de saisie des résultats.
* Abandon du support PostGreSQL.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.


v1.11 (28/02/2010)
==================

* Ajout de l'affichage des résultats au public WEB GOOGLE identique à 
  l'affichage WEB existant avec une carte Google Map.
* Ajout de l'affichage des résultats au public MOBILE pour visualisation 
  adaptée aux smartphones.
* Mise à jour réglementaire de l'export préfecture selon les nouvelles 
  modalités.


v1.09 & v1.10 (23/06/2009)
==========================

* Ajout du support de la seconde centaine.


